## General Information

This is the data repository for the time series simulation results of the 
publication Bozionek et al. - Design and Evaluation of a Multi-level Reactive Power Market.
https://www.researchsquare.com/article/rs-1389372/v1. (preprint)

The code that was used to generate the data can be found here: https://gitlab.com/digitalized-energy-systems/scenarios/multi_level_reactive_power_market_design

For questions, suggestions, etc. contact Thomas Wolgast per email
(thomas.wolgast@uni-oldenburg.de) or create an issue here.


## Data Structure

The result data for the two reference cases and the reactive power market is 
divided into the following folders:

* The folder `market` contains the results for the reactive power market
* The folder `ideal_optimum` contains the results for the reference case RC_OptMarket
* The folder `status_quo` contains the results for the reference case RC_NoMarket

As described in the paper, we performed a power flow calculation with a continuous tap optimization 
for the complete grid model. However, in each of the data folders you can find the results 
for simulations with a discrete tap optimization (dt), no tap optimization (nt) and the used data of the continuous tap optimization (ct).

### Content of the results CSV files
The naming of the CSV result files is the same for the different simulation methods (Market, RC_OptMarket, RC_NoMarket).
The name of each CSV file starts with the particular simulation method/case used, followed by the tap optimization method used, and then the results that are contained.  
For the market, the respective EPF approximation method is also included in the name of the respective CSV file. 

The specifying names of the results files for a time series simulation are the following:
* "_Simulation method_"_"_tap optimization method_": Contains different simulation results, like costs, active power losses, reactive power losses, provided reactive power etc.
* "_Simulation method_"_"_tap optimization method_"_loading_l: Contains all line loadings
* "_Simulation method_"_"_tap optimization method_"_l_max: Contains the loadings of the highest loaded line of each grid
* "_Simulation method_"_"_tap optimization method_"_loading_t: Contains all transformer loadings
* "_Simulation method_"_"_tap optimization method_"_loading_t_max: Contains the loading of the highest loaded transformer of each grid
* "_Simulation method_"_"_tap optimization method_"_voltage: Contains all node voltages of each grid 
* "_Simulation method_"_"_tap optimization method_"_voltage_min_max: Contains the minimum and maximum node voltage of each grid
